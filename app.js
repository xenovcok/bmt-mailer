const env = process.env;
const path = require('path');
const passport = require('passport');
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const mongoose =  require('mongoose');
const morgan = require('morgan');
const mongoStore = require('connect-mongo')(session);
const favicon = require('serve-favicon');
var flash    = require('connect-flash');
var hour = 3600000;

const app = express();

var ip_addr = process.env.OPENSHIFT_NODEJS_IP   || '127.0.0.1';
var port    = process.env.OPENSHIFT_NODEJS_PORT || '3030';

var connection_string = '127.0.0.1:27017/broadcaster';
// if OPENSHIFT env variables are present, use the available connection info:
if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
  connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
  process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
  process.env.OPENSHIFT_APP_NAME;
}

mongoose.connect(connection_string, {config: {useMongoClient: true}});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

require('./config/passport')(passport);

app.set('view engine', 'twig');
app.set('views', './views');
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(__dirname+'/public/images/favicon.ico'));
app.set('twig options', { 
  strict_variables: false
});

app.use(morgan('dev')); 
app.use(cookieParser());
app.use(session({
	secret: '1234657890',
	resave: true,
	saveUninitialized: true,
	store: new mongoStore({ mongooseConnection: mongoose.connection }),
	cookie: {
		expires: new Date(Date.now() + hour),
		maxAge: hour,
		secure: false
	}
}));

/*app.use(function printSession(req, res, next) {
  console.log('req.session', req.session);
  return next();
}); */

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash());

require('./router/bmt.router')(app, passport);

app.get('*', function(req, res){
  res.redirect('/');
});

app.listen( port, ip_addr, function () {
  console.log(`Application worker ${process.pid} started...`);
});