var LocalStrategy   = require('passport-local').Strategy;


var User =  require('../models/user.js').User;

module.exports = function(passport) {
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

    passport.deserializeUser(function(id, done) {
    	User.findById(id, function(err, user) {
    		done(err, user);
    	});
    });

    passport.use('local-login', new LocalStrategy({
     
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, email, password, done) {

        User.findOne({ 'email' :  email }, function(err, user) {
            console.log(user);
            if (err)
            	return done(err);
           
            if (!user)
                return done(null, false, {message:'Email tidak Terdaftar'}); 

            if (password != user.password)
                return done(null, false, {message:'Password Salah'}); 
            // all is well, return successful user
            return done(null, user);
        });

    }));
}